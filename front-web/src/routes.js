import React from "react";
import { Switch, Route } from "react-router-dom";
import FeedContainer from "./pages/Feed";
import NewContainer from "./pages/New";

function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={FeedContainer} />
      <Route path="/new" component={NewContainer} />
    </Switch>
  );
}
export default Routes;
