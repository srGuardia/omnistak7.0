const Post = require("../models/Post");
// manipula as imagens
const sharp = require("sharp");
const path = require("path");
const fs = require("fs");

module.exports = {
  // rota de listagem dos posts
  async index(req, res) {
    const post = await Post.find().sort("-createdAt");

    return res.json(post);
  },

  // rota para adicionar (publicar) os posts
  async store(req, res) {
    const { author, place, description, hashtags } = req.body;
    const { filename: image } = req.file;

    // salvar imagens como .jpg (name => tudo antes do '.')
    const [name] = image.split(".");
    const filename = `${name}.jpg`;

    // fazendo o tratamento da imagem
    await sharp(req.file.path)
      .resize(500)
      .jpeg({ quality: 70 })
      .toFile(path.resolve(req.file.destination, "resized", filename));

    // deletando imagem antiga não tratada
    fs.unlinkSync(req.file.path);

    const post = await Post.create({
      author,
      place,
      description,
      hashtags,
      image: filename
    });

    // avisa todos os usuários ativos em real time
    req.io.emit("post", post);

    return res.json(post);
  }
};
