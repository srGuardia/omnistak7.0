const Post = require("../models/Post");

module.exports = {
  // rota para adicionar (publicar) os posts
  async store(req, res) {
    // recuperar o id do post enviado pela rota
    const post = await Post.findById(req.params.id);

    // ele pega o valor do like e soma
    post.likes += 1;

    // salva
    await post.save();

    req.io.emit("like", post);

    return res.json(post);
  }
};
