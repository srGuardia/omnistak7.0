const express = require("express");
const multer = require("multer");
// importando para fazer teste
const uploadsConfig = require("./config/upload");
// importando o controlle para regra de negócio
const PostController = require("./controllers/PostController");
const LikeController = require("./controllers/LikeController");

const routes = new express.Router();
const upload = multer(uploadsConfig);

// criando a rota de posts e a função desejada
routes.post("/posts", upload.single("image"), PostController.store);

// rota para listagem de posts
routes.get("/posts", PostController.index);

// rota para like no post desejado
routes.post("/posts/:id/like", LikeController.store);

module.exports = routes;
