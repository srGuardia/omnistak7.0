const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
const cors = require("cors");

const app = express();

// fazer o express reconhecer tanto HTTP quando Web.Socket
const server = require("http").Server(app);
// permitir que o backend forneça os dados em (real time)
const io = require("socket.io")(server);

// Aqui é feita a conexão com o banco de dados
mongoose.connect(
  "mongodb+srv://breno:karina01@cluster0-9unbu.mongodb.net/test?retryWrites=true&w=majority",
  {
    useNewUrlParser: true
  }
);

// criando próprio midleware
app.use((req, res, next) => {
  req.io = io;

  next();
});

// permite que o backend seja acessivel em qualquer lugar
app.use(cors());

// quando a rota for /files o front-end irá conseguir visualizar a imagem desejada
app.use(
  "/files",
  express.static(path.resolve(__dirname, "..", "uploads", "resized"))
);

// Importa as rotas
app.use(require("./routes"));

// Caminho em que o servidor vai rodar
server.listen(3333);
